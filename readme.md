How to... 


1)  Log in to your Discord account

2)  Goto http://discordapp.com/developers/applications/me
    and click "Create Application".

3)  Copy Client Id and Client Secret

4)  Click Bot in the menu on the left and add a bot

5)  Click to reveal token and copy it

6)  Set permissions to at least "Send Messages" 
    and copy the permission integer      

7)  To apply your bot to your server, to go 
    https://discordapp.com/oauth2/authorize?&client_id=CLIENT_ID&scope=bot&permissions=PERMISSION_INTEGER

8)  Run `npm install` 

9)  Modify auth.json to match your token

10) Run the server, by running `node index.js`

11) Some sample replies has been added, but feel free to add your own
