var auth = require('./auth.json');
var bot = require('./Bot.js');
    bot = new bot();


// The command character decides what a command must be prefixed with. 
// Use an uncommen charater, like !, # or // 
// Multiple characters are allowed
bot.setCommandCharacter('!');

// Register a command with a callback. 
// Do not include the command character when registering commands
bot.registerCommand('ping', function(user, userId, channelId, message, args, evt ){
    bot.sendMessage(channelId, "pong");
});

// Pleease note that commands are case-sensitive
bot.registerCommand('hello', function(user, userId, channelId, message, args, evt){
    bot.sendMessage(channelId, 'world');
});

// When using commands, you will get a list of arguments, which is the message after the command
// In this case, if you send "!greet santa", the response will be "Hello, santa"
bot.registerCommand('greet', function(user, userId, channelId, message, args, evt){
    var message = "";
    if(args.length == 0 || args.length > 1){
        message = "The !hello command expects exactly ONE parameter";
    }else{
        message = "Hello, " + args[0];
    }

    bot.sendMessage(channelId, message);

});

// Register a general callback, that will intercept all messages.
// The evt is the event that is triggered.
// It holds information like timestamp, and attached files
bot.registerCallback(function(user, userid, channel, message, evt){
    console.log(user + "@" + channel + ":" + message);
    console.log(JSON.stringify(evt));
    console.log('');// Blank line
});

// Connect to Discord, using a token. The token can be set in auth.json
bot.connect(auth.token);

// Start listening for incoming message
// If you just want to send a message to your channel, this is not necessary.
bot.startListen();

