
var Discord = require('discord.io');

var Bot = function(){
    this.commands = [];
    this.commandCharacter = "!";
    this.attachmentHandler = false
    this.callback = false;
    this.client = false;
}


Bot.prototype.setCommandCharacter = function(character){
    this.commandCharacter = character;
}

Bot.prototype.registerCommand = function(command, func){
    this.commands[command] = func;
}

Bot.prototype.knowsCommand = function(command){
    return (command in this.commands);
}

Bot.prototype.isCommand = function(msg){
    return msg.substring(0,1) == this.commandCharacter; 
}

Bot.prototype.getReply = function(command, args){
    var func = this.commands[command];
    return func(args);
}

Bot.prototype.registerCallback = function(callback){
    console.log("Callback function registered");
    this.callback = callback;
}

Bot.prototype.sendMessage = function(channelID, message){
    console.log("Sending " + message);
    this.client.sendMessage({
        to: channelID,
        message: message
    });
}

Bot.prototype.connect = function(token){
    this.client = new Discord.Client({
        token: token,
        autorun: true
    });
}

Bot.prototype.handleCommand = function(user, userId, channelId, cmd, args, evt){
    var func = this.commands[cmd];
    func(user, userId, channelId, cmd, args, evt)
}

Bot.prototype.startListen = function(){
    let self = this;

    this.client.on('ready', function (evt) {
        console.log('Logged in as: ' + self.client.username + ' - (' + self.client.id + ')');
    });

    this.client.on('message', function (user, userID, channelID, message, evt) {
        
        if(self.callback !== false){
            self.callback(user, userID, channelID, message, evt);
        }

        if (self.isCommand(message)) {
            var args = message.substring(1).split(' ');
            var cmd = args[0];
                args = args.splice(1);
            if(self.knowsCommand(cmd)){
                self.handleCommand(user, userID, channelID, cmd, args, evt);
            }else{
                console.log("Unknown command: " + cmd);
            }
         }
    });
}

module.exports =  Bot;
